from django.contrib.auth import login, get_user_model, logout
from django.views.decorators.http import require_http_methods
from django.views.generic import TemplateView, RedirectView
from django.http import JsonResponse
from django.core.urlresolvers import reverse

from accounts.forms import SignInForm

User = get_user_model()


@require_http_methods(['POST'])
def user_signin(request):
    form = SignInForm(data=request.POST)
    if form.is_valid():
        login(request, form.user_cache)
        return JsonResponse(dict(result=True, next_url=reverse('new_pubs')))
    else:
        return JsonResponse(dict(result=False, errors=form.errors))


class SignInPageView(TemplateView):

    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = SignInForm()
        return context


class LogoutView(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        logout(self.request)
        return reverse('signin')
