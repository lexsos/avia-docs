from django import forms

from django.contrib.auth import get_user_model, authenticate

User = get_user_model()


class SignInForm(forms.Form):

    login = forms.CharField(error_messages=dict(required='Введите логин'))
    password = forms.CharField(widget=forms.PasswordInput, error_messages=dict(required='Введите пароль'))

    error_message = 'Пользователь с указанными логином и паролем не найден или не активирован.'

    def clean(self):
        cleaned_data = super().clean()
        if cleaned_data.get('login') and cleaned_data.get('password'):
            try:
                user = User.objects.get(login__iexact=cleaned_data.get('login', ''), is_active=True)
                self.user_cache = authenticate(login=user.login, password=cleaned_data.get('password'))
                if self.user_cache is None:
                    raise forms.ValidationError(self.error_message)
            except User.DoesNotExist:
                raise forms.ValidationError(self.error_message)
