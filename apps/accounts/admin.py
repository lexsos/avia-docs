from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import ugettext_lazy as _

from accounts.models import User


class UserAdmin(BaseUserAdmin):

    list_display = ('login', 'surname', 'name', 'patronymic', 'is_active', 'is_staff')
    list_filter = ('is_staff', 'is_superuser', 'is_active')
    ordering = ('login', )

    def get_fieldsets(self, request, obj=None):
        main_fields = ['login', 'password', 'surname', 'name', 'patronymic']
        permissions_fields = ['is_staff', 'is_superuser', 'is_active', 'groups', 'user_permissions']
        if not obj:
            main_fields.remove('password')
            main_fields.insert(1, 'password1')
            main_fields.insert(2, 'password2')
        return [
            (None, {'fields': main_fields}), (_('Permissions'), {'fields': permissions_fields})]


admin.site.register(User, UserAdmin)
