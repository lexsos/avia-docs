from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.db import models


class UserManager(BaseUserManager):

    def _create_user(self, login, password, is_staff, is_superuser, **extra_fields):
        if not login:
            raise ValueError('Пользователь должн иметь логин')
        login = login.lower().strip()
        user = self.model(login=login, is_staff=is_staff, is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, login, password=None, **extra_fields):
        return self._create_user(login, password, False, False, **extra_fields)

    def create_superuser(self, login, password, **extra_fields):
        return self._create_user(login, password, True, True, **extra_fields)

    def active(self):
        return self.filter(is_active=True)


class User(AbstractBaseUser, PermissionsMixin):

    login = models.CharField('Логин', max_length=200, unique=True)
    name = models.CharField('Имя', max_length=200)
    surname = models.CharField('Фамилия', max_length=200)
    patronymic = models.CharField('Отчество', max_length=200)
    is_staff = models.BooleanField(
        'Статус персонала', default=False,
        help_text='Отметьте, если пользователь может входить в административную часть сайта.')
    is_active = models.BooleanField('Активен', default=True)

    objects = UserManager()

    USERNAME_FIELD = 'login'
    REQUIRED_FIELDS = ['name', 'surname', 'patronymic']

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'
        ordering = ['login', ]

    def __str__(self):
        return self.get_full_name()

    def get_full_name(self):
        return '{} {} {}'.format(self.surname, self.name, self.patronymic)

    def get_short_name(self):
        return self.login
