from django.conf.urls import url

from accounts import views


urlpatterns = [
    url(r'^api/accounts/signin$', views.user_signin, name='api_signin'),
    url(r'^signin$', views.SignInPageView.as_view(), name='signin'),
    url(r'^logout$', views.LogoutView.as_view(), name='logout'),
]
