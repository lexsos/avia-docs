# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-09-15 08:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
        ('reports', '0002_report_add_signature'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='groups',
            field=models.ManyToManyField(blank=True, related_name='reports', to='auth.Group', verbose_name='Группы'),
        ),
    ]
