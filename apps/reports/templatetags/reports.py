from django import template

from publications.models import Feed


register = template.Library()


@register.assignment_tag()
def doc_see_on(user, publication):
    feed = Feed.objects.filter(user=user, publication=publication).first()
    if feed:
        return feed.see_on
