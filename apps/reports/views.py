from functools import reduce

from django.views.generic import TemplateView
from django.shortcuts import get_object_or_404
from django.contrib.auth import get_user_model
from django.template import loader
from django.db.models import Q
from django.contrib.auth.models import Group
from weasyprint import HTML
from django.http import HttpResponse

from publications.models import Publication

User = get_user_model()


class ReportView(TemplateView):

    template_name = 'reports/preview.html'
    content_type = 'application/pdf'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['signature'] = True if self.kwargs['signature'] == 'on' else False
        context['publication'] = get_object_or_404(Publication, pk=self.kwargs['publication'])
        context['user_list'] = self._get_users_qs().order_by('surname', 'name', 'patronymic')
        return context

    def get(self, request, *args, **kwargs):
        response = HttpResponse(self.render_pdf(), content_type=self.content_type)
        return response

    def render_pdf(self):
        context = self.get_context_data()
        html_doc = loader.render_to_string(self.template_name, context)
        return HTML(string=html_doc).write_pdf()

    def _get_users_qs(self):
        users = self.kwargs['users']
        groups = self.kwargs['groups']
        filter_args = []
        if users:
            filter_args.append(Q(pk__in=users.split(',')))
        if groups:
            groups_qs = Group.objects.filter(pk__in=groups.split(','))
            filter_args.append(Q(groups__in=groups_qs))
        if filter_args:
            return User.objects.filter(reduce(lambda x,y: x|y, filter_args)).distinct()
        return User.objects.filter(is_staff=False, is_superuser=False)
