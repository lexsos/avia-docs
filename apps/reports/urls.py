from django.conf.urls import url

from reports.views import ReportView


urlpatterns = [
    url(
        r'^report/publication/(?P<publication>\d+)'
        r'(?:/users/(?P<users>((\d+(,\d+)*)|all)))?'
        r'(?:/groups/(?P<groups>((\d+(,\d+)*))))?'
        r'/signature/(?P<signature>(on|off))$',
        ReportView.as_view(), name='index'),
]
