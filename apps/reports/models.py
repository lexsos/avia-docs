from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from publications.models import Publication

User = get_user_model()


class Report(models.Model):

    title = models.CharField(verbose_name='Название', max_length=255)
    publications = models.ForeignKey(Publication, verbose_name='Публикация', related_name='reports')
    users = models.ManyToManyField(User, verbose_name='Пользователи', related_name='reports', blank=True)
    groups = models.ManyToManyField(Group, verbose_name='Группы', related_name='reports', blank=True)
    use_all_users = models.BooleanField(verbose_name='Все пользователи', default=False)
    add_signature = models.BooleanField(verbose_name='Добавить поле "Подпись"', default=True)

    class Meta:
        verbose_name_plural = 'Отчеты'
        verbose_name = 'Отчет'
        ordering = ['title', '-pk']
