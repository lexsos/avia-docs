from django.contrib import admin
from easy_select2 import select2_modelform

from reports.models import Report


ReportForm = select2_modelform(Report, attrs={'width': '250px'})


class ReportAdmin(admin.ModelAdmin):

    form = ReportForm
    list_display = ('title', )
    change_form_template = 'admin/report_change.html'
    fields = ('title', 'publications', 'add_signature', 'use_all_users', 'users', 'groups')


admin.site.register(Report, ReportAdmin)
