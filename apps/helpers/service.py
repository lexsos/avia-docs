from collections import OrderedDict
import urllib.parse as urlparse
from urllib.parse import urlencode


def add_url_get_parameters(url, **kwargs):
    url_parts = list(urlparse.urlparse(url))
    query = OrderedDict(urlparse.parse_qsl(url_parts[4]))
    query.update(kwargs)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)
