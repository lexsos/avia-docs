from django.contrib import admin
from django import forms
from easy_select2 import select2_modelform_meta

from publications.models import Publication, Attachment, Feed
from helpers.admin import AdminTinymceMixin
from publications.forms import AttachmentAdminForm


class AttachmentInline(admin.TabularInline):

    model = Attachment
    extra = 0
    form = AttachmentAdminForm


class PublicationAdminForm(forms.ModelForm):

    Meta = select2_modelform_meta(Publication)

    def clean(self):
        cleaned_data = super().clean()
        publish_to_all = cleaned_data.get('publish_to_all')
        if (not publish_to_all) and (not cleaned_data.get('groups')):
            raise forms.ValidationError('Выберите группы для публикации или "Опубликовать для всех"')
        if publish_to_all:
            cleaned_data['groups'] = list()
        return cleaned_data


class PublicationAdmin(AdminTinymceMixin, admin.ModelAdmin):

    list_filter = ('is_show_on_site', )
    list_display = ('title', 'pub_date', 'is_show_on_site')
    inlines = [AttachmentInline]
    rich_fields = ['description']
    change_form_template = 'admin/publication_change.html'
    form = PublicationAdminForm


admin.site.register(Publication, PublicationAdmin)
