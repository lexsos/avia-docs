from django.views.generic import ListView, TemplateView, View
from django.http import JsonResponse
from django.template import loader
from django.urls import reverse
from django.shortcuts import get_object_or_404
from django.utils import timezone
from django.utils.dateparse import parse_datetime
from django.db.models import Q

from publications.models import Publication, Feed
from helpers.service import add_url_get_parameters
from helpers.views import LoginRequiredMixin


class AllPublicationsView(LoginRequiredMixin, TemplateView):

    template_name = 'publications/all.html'
    load_url_name = 'all_pubs_load'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        load_date = timezone.now().isoformat()
        context['next_url'] = add_url_get_parameters(reverse(self.load_url_name), load_date=load_date)
        return context


class NewPublicationsView(AllPublicationsView):

    template_name = 'publications/new.html'
    load_url_name = 'new_pubs_load'


class AllPublicationsLoadView(LoginRequiredMixin, ListView):

    paginate_by = 10
    template_name = '_blocks/_publications.html'
    url_name = 'all_pubs_load'

    def get_queryset(self):
        return Publication.objects.public()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['user'] = self.request.user
        return context

    def get_next_url(self, page):
        if page.has_next():
            return add_url_get_parameters(reverse(self.url_name), page=page.next_page_number())

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data()
        page = context['page_obj']
        html = loader.render_to_string(self.template_name, context)
        response_data = {'html': html, 'has_next': page.has_next()}
        next_url = self.get_next_url(page)
        if next_url:
            response_data['next_url'] = next_url
        return JsonResponse(response_data)


class NewPublicationsLoadView(AllPublicationsLoadView):

    url_name = 'new_pubs_load'

    def get_next_url(self, page):
        next_url = super().get_next_url(page)
        load_date = self.request.GET.get('load_date', '')
        if next_url and load_date:
            next_url = add_url_get_parameters(next_url, load_date=load_date)
        return next_url

    def get_queryset(self):
        load_date = None
        try:
            load_date = parse_datetime(self.request.GET.get('load_date', ''))
        except ValueError:
            pass
        see_filter = Q(feeds__see_on__isnull=True)
        if load_date:
            see_filter |= Q(feeds__see_on__gte=load_date)
        return Publication.objects.public().filter(see_filter, feeds__user=self.request.user).distinct()


class SignatureView(LoginRequiredMixin, View):

    template_name = '_blocks/_publications.html'

    def post(self, request, *args, **kwargs):
        publication = get_object_or_404(Publication, id=kwargs['publication'])
        feed, _ = Feed.objects.get_or_create(publication=publication, user=self.request.user)
        result = dict(updated=False)
        if not feed.see_on:
            feed.see_on = timezone.now()
            feed.save()
            result['updated'] = True
        context = dict(publication_list=[feed.publication], user=self.request.user)
        result['html'] = loader.render_to_string(self.template_name, context)
        return JsonResponse(result)
