from django.conf.urls import url

from publications.views import AllPublicationsView, AllPublicationsLoadView, NewPublicationsLoadView, \
    NewPublicationsView, SignatureView


urlpatterns = [
    url(r'^$', NewPublicationsView.as_view(), name='new_pubs'),
    url(r'^all$', AllPublicationsView.as_view(), name='all_pubs'),
    url(r'^api/all_load$', AllPublicationsLoadView.as_view(), name='all_pubs_load'),
    url(r'^api/new_load$', NewPublicationsLoadView.as_view(), name='new_pubs_load'),
    url(r'^api/signature/(?P<publication>\d+)$', SignatureView.as_view(), name='signature'),
]
