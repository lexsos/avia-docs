from django.db import models
from django.utils import timezone
from django.contrib.auth import get_user_model
from django.db.models import query, Manager
from django.contrib.auth.models import Group

User = get_user_model()


class PublicQuerySet(query.QuerySet):

    def public(self):
        return self.filter(is_show_on_site=True)


class PublicManager(Manager):

    def __getattr__(self, attr, *args):
        """ Allow to use queryset methods from custom queryset """
        try:
            return getattr(self.__class__, attr, *args)
        except AttributeError:
            # don't delegate internal methods to the queryset
            if attr.startswith('__') and attr.endswith('__'):
                raise
            return getattr(self.get_queryset(), attr, *args)

    def get_queryset(self):
        return getattr(self.model, 'QuerySet', PublicQuerySet)(self.model, using=self._db)


class Publication(models.Model):

    title = models.CharField(verbose_name='Заголовок', max_length=255)
    description = models.TextField(verbose_name='Описание', blank=True, null=True)
    pub_date = models.DateTimeField(verbose_name='Дата публикации', default=timezone.now)
    is_show_on_site = models.BooleanField(verbose_name='Показывать на сайте', default=True)
    publish_to_all = models.BooleanField(verbose_name='Опубликовать для всех', default=False)
    groups = models.ManyToManyField(Group, verbose_name='Группы', blank=True)

    objects = PublicManager()

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Публикации'
        verbose_name = 'Публикация'
        ordering = ['-pub_date']


class Attachment(models.Model):

    FILE, URL = ('file', 'url')
    TYPE_CHOICES = ((FILE, 'Файл'), (URL, 'Ссылка'))

    title = models.CharField(verbose_name='Название', max_length=255)
    publication = models.ForeignKey(Publication, verbose_name='Публикация', related_name='attachments')
    type = models.CharField(verbose_name='Публикация', choices=TYPE_CHOICES, max_length=10)
    file = models.FileField(upload_to='publications', verbose_name='Файл', blank=True, null=True)
    url = models.URLField(verbose_name='Ссылка', blank=True, null=True)
    order = models.PositiveIntegerField(verbose_name='Порядковый номер', default=0)

    def get_absolute_url(self):
        if self.type == self.FILE:
            return self.file.url
        elif self.type == self.URL:
            return self.url

    class Meta:
        verbose_name_plural = 'Приложения'
        verbose_name = 'Приложение'
        ordering = ['-publication', 'order', 'pk']


class Feed(models.Model):

    user = models.ForeignKey(User, verbose_name='Пользователь')
    publication = models.ForeignKey(Publication, verbose_name='Публикация', related_name='feeds')
    see_on = models.DateTimeField(verbose_name='Дата ознакомления', blank=True, null=True)

    def __str__(self):
        return '{} {}'.format(self.user.get_full_name(), self.publication.title)

    class Meta:
        verbose_name_plural = 'Ленты'
        verbose_name = 'Лента'
        ordering = ['-see_on', '-pk']
        unique_together = ('user', 'publication')
