from django.db.models import Q
from django.contrib.auth import get_user_model

from publications.models import Feed

User = get_user_model()


def publish_to_users(publication, users):
    for user in users:
        Feed.objects.get_or_create(user=user, publication=publication)


def publish_to_groups(publication):
    users = User.objects.filter(groups=publication.groups.all()).distinct()
    publish_to_users(publication, users)
    Feed.objects.filter(~Q(user__in=users), publication=publication, see_on__isnull=True).delete()
