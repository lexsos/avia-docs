from django.db.models.signals import pre_save, post_save, m2m_changed

from django.dispatch import receiver
from django.contrib.auth import get_user_model

from publications.models import Attachment, Publication
from publications.service import publish_to_users, publish_to_groups
from reports.models import Report

User = get_user_model()


@receiver(pre_save, sender=Attachment)
def clear_attachment_file(instance, **kwargs):
    if instance.type == Attachment.FILE:
        return
    instance.file = None


@receiver(post_save, sender=Publication)
def make_feed(instance, **kwargs):
    if instance.publish_to_all:
        publish_to_users(instance, User.objects.all())


@receiver(m2m_changed, sender=Publication.groups.through)
def make_feed_for_groups(instance, action, **kwargs):
    if not isinstance(instance, Publication):
        return
    if (action in ['post_add', 'post_remove', 'post_clear']) and (not instance.publish_to_all):
        publish_to_groups(instance)


@receiver(post_save, sender=Publication)
def auto_create_report(instance, created, **kwargs):
    if created:
        Report.objects.create(title=instance.title, publications=instance, use_all_users=True)
