from django import template

from publications.models import Feed


register = template.Library()


@register.assignment_tag()
def is_signed_publications(user, publication):
    return Feed.objects.filter(user=user, publication=publication, see_on__isnull=False).exists()
