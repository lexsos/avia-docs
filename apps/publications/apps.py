from django.apps import AppConfig


class PublicationsConfig(AppConfig):

    name = 'publications'
    verbose_name = "Публикации"

    def ready(self):
        import publications.signals  # noqa
