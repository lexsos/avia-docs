from django import forms

from publications.models import Attachment


class AttachmentAdminForm(forms.ModelForm):

    FILE_EMPTY_ERROR = 'Выберите файл'
    URL_EMPTY_ERROR = 'Укажите ссылку'

    class Meta:
        model = Attachment
        fields = 'publication', 'type', 'title', 'file', 'url', 'order'

    def clean_file(self):
        file = self.cleaned_data['file']
        attachment_type = self.cleaned_data['type']
        if attachment_type != Attachment.FILE:
            return None
        if not file:
            raise forms.ValidationError(self.FILE_EMPTY_ERROR)
        return file

    def clean_url(self):
        url = self.cleaned_data['url']
        attachment_type = self.cleaned_data['type']
        if attachment_type != Attachment.URL:
            return None
        if not url:
            raise forms.ValidationError(self.URL_EMPTY_ERROR)
        return url
