SECRET_KEY = '!fb(0qc_7b8pp$fmvxn3f*3ee=9_8u69*#(1c)smibg-@(40^i'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': '{{ db_name }}',
        'USER': '{{ db_user }}',
        'PASSWORD': '{{ db_password }}',
        'HOST': '127.0.0.1',
    }
}

ALLOWED_HOSTS = ['*']

DEBUG = False
