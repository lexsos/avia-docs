import os

import fabric
from fabric.state import env
from fabric.api import run, sudo, cd, roles


BASE_DIR = os.path.dirname(__file__)
TEMPLATE_DIR = os.path.join(BASE_DIR, 'conf')
VARS = {
    'project_name': 'avia-docs',
    'zabbix_server': '10.1.1.107',
    'repo': 'https://lexsos@bitbucket.org/lexsos/avia-docs.git',
    'home_dir': '/home/aviadoc',
    'proj_dir': '/home/aviadoc/avia-docs',
    'pip': '/home/aviadoc/env/bin/pip',
    'python': '/home/aviadoc/env/bin/python',
    'gunicorn': '/home/aviadoc/env/bin/gunicorn',
    'public_dir': '/home/aviadoc/avia-docs/var/public',
    'media_dir': '/home/aviadoc/avia-docs/var/public/media',
    'static_dir': '/home/aviadoc/avia-docs/var/public/static',
    'db_user': 'avia_docs_user',
    'db_name': 'avia_docs',
    'db_password': '123456',
    'http_port': '80',
    'http_host': '0.0.0.0',
    'user_name': 'aviadoc',
}

env.roledefs = {
    'beta': ['root@10.1.3.21'],
    'prod': ['root@10.1.3.22'],
}


def common_pkg():
    sudo('apt-get -y update')
    sudo('apt-get -y upgrade')
    sudo('apt-get -y install libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev git libffi-dev curl')


def install_nginx():
    sudo('apt-get -y install nginx')
    sudo('systemctl enable nginx')
    sudo('systemctl restart nginx')


def intall_database():
    sudo('apt-get -y install postgresql postgresql-server-dev-all python-psycopg2')


def install_zabbix_agent():
    sudo('apt-get -y install zabbix-agent')
    fabric.contrib.files.upload_template(
        'zabbix/zabbix_agentd.conf', '/etc/zabbix/zabbix_agentd.conf',
        context=VARS, use_jinja=True, backup=False, use_sudo=True, template_dir=TEMPLATE_DIR)
    sudo('systemctl enable zabbix-agent.service')
    sudo('systemctl restart zabbix-agent')


def install_python():
    sudo('apt-get -y install python3 python3-pip python3-virtualenv libpango1.0-0 libcairo2 libpq-dev')
    sudo('apt-get -y build-dep python3 python3-pip python3-virtualenv')


def add_user():
    with fabric.context_managers.settings(warn_only=True):
        sudo('useradd aviadoc')
        sudo('mkdir /home/aviadoc')
    sudo('chown -R aviadoc:aviadoc /home/aviadoc')


def add_service_file():
    fabric.contrib.files.upload_template(
        'systemd/avia-docs.service', '/etc/systemd/system/avia-docs.service',
        context=VARS, use_jinja=True, backup=False, use_sudo=True, template_dir=TEMPLATE_DIR)
    sudo('systemctl daemon-reload')
    sudo('systemctl enable avia-docs.service')


@roles('beta')
def init_beta():
    common_pkg()
    install_nginx()
    install_zabbix_agent()
    install_python()
    intall_database()
    add_user()
    add_service_file()


@roles('beta')
def init_app_beta():
    with fabric.context_managers.settings(warn_only=True):
        db_commands = [
            "CREATE USER {db_user} WITH PASSWORD '{db_password}';",
            "CREATE DATABASE {db_name} OWNER {db_user};"
        ]
        for command in db_commands:
            command = command.format(**VARS)
            sudo('psql -c "{}"'.format(command), user='postgres')
    with fabric.context_managers.settings(warn_only=True), cd(VARS['home_dir']):
        sudo('git clone {}'.format(VARS['repo']))
        sudo('python3 -m venv --without-pip env')
        sudo('curl https://bootstrap.pypa.io/get-pip.py | {python}'.format(**VARS))
        sudo('mkdir -p {media_dir}'.format(**VARS))
        sudo('mkdir -p {static_dir}'.format(**VARS))


def deploy():
    # local settings
    fabric.contrib.files.upload_template(
        'settings_local.py', '{proj_dir}/project/settings_local.py'.format(**VARS),
        context=VARS, use_jinja=True, backup=False, use_sudo=True, template_dir=TEMPLATE_DIR)
    with cd(VARS['proj_dir']):
        sudo('git pull')
        sudo('{pip} install --upgrade pip'.format(**VARS))
        sudo('{pip} install -r requirements.txt'.format(**VARS))
        sudo('{python} manage.py migrate --noinput'.format(**VARS))
        sudo('{python} manage.py collectstatic --noinput'.format(**VARS))
    # nginx config
    fabric.contrib.files.upload_template(
        'nginx/host.conf', '/etc/nginx/sites-available/{project_name}'.format(**VARS),
        context=VARS, use_jinja=True, backup=False, use_sudo=True, template_dir=TEMPLATE_DIR)
    sudo('ln -sf /etc/nginx/sites-available/{project_name} /etc/nginx/sites-enabled/{project_name}'.format(**VARS))
    sudo('systemctl restart nginx')
    # restart workers
    sudo('chown -R aviadoc:aviadoc /home/aviadoc')
    sudo('systemctl restart avia-docs.service')


@roles('beta')
def deploy_beta():
    deploy()


@roles('prod')
def deploy_prod():
    deploy()
