(function($) {

    var attachment_inputs = ['.field-url input', '.field-file input'];
    var attachment_types = ['url', 'file'];

    var hide_attachment_input = function (attachment) {
        attachment_inputs.forEach(function (val) {
            $(attachment).find(val).addClass('publication-change-hide');
        });
    };

    var attachment_inline = function () {
        $('.dynamic-attachments').each(function (i, e) {
            hide_attachment_input(e);
            var attachment_type = $(e).find('.field-type select').val();
            var type_index =  $.inArray(attachment_type, attachment_types);
            if (type_index >= 0){
                $(e).find(attachment_inputs[type_index]).removeClass('publication-change-hide');
            }
        });
    };

    var groups = function () {
        var div_groups = $('.field-groups');
        if ($('#id_publish_to_all').prop('checked')){
            div_groups.hide();
        }
        else {
            div_groups.show();
        }
    };

    $(document).on('formset:added', function(event, $row, formsetName) {
        if (formsetName == 'attachments') {
            attachment_inline();
        }
    });

    $(document).ready(function() {

        $('.field-type select').change(function () {
            attachment_inline();
        });

        $('#attachments-group').on('change', '.field-file input', function () {
            var filename = $(this).val().split('\\').pop();
            filename = filename.substring(0, filename.lastIndexOf('.'));
            var title = $(this).parent().parent().find('.field-title input');
            if (!title.val()){
                title.val(filename);
            }
        });

        $('#id_publish_to_all').change(function () {
            groups();
        });

        attachment_inline();
        groups();
    });
})(django.jQuery);
