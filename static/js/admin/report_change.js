(function($) {

    var is_all_users = function () {
        return $('.field-use_all_users input').prop('checked');
    };

    var users_input = function (attachment) {
        var users = [ $('.field-users'),  $('.field-groups')];
        if (is_all_users()){
            users.forEach(function (el) {
                $(el).hide();
            })
        }
        else {
            users.forEach(function (el) {
                $(el).show();
            })
        }
    };

    var add_pdf_button = function () {
        $('.submit-row').append('<input value="PDF" name="" class="pdf-button" type="submit">');
    };

    var get_user_arg = function () {
        var selectedUsers = [];
        $('#id_users').find('option:selected').each(function () {
            var userId = $(this).prop('value');
            selectedUsers.push(userId);
        });
        return selectedUsers.join();
    };

    var get_groups_arg = function () {
        var selectedGroups = [];
        $('#id_groups').find('option:selected').each(function () {
                var groupId = $(this).prop('value');
                selectedGroups.push(groupId);
            });
            return selectedGroups.join();
    };

    var get_publication_arg = function () {
        return $('#id_publications').val();
    };

    var pdf_click = function (event) {
        event.preventDefault();
        var is_all = is_all_users();
        var users = get_user_arg();
        var groups = get_groups_arg();
        var publication = get_publication_arg();
        var signature = $('#id_add_signature').prop('checked')
        if (!users && !groups && !is_all){
            alert('Укажите пользователей');
            return;
        }
        if (!publication){
            alert('Укажите публикацию');
            return;
        }
        var url = '/report/publication/' + publication;
        if (!is_all) {
            if (users) {
                url += '/users/' + users;
            }
            if (groups) {
                url += '/groups/' + groups;
            }
        }
        url += '/signature/';
        if (signature){
            url += 'on';
        }
        else {
            url += 'off';
        }
        window.open(url, '_blank');
    };

    $(document).ready(function() {

        $('.field-use_all_users input').change(function () {
            users_input();
        });

        $('.submit-row').on('click', '.pdf-button', pdf_click);

        users_input();
        add_pdf_button();
    });
})(django.jQuery);
