$( document ).ready(function() {

    var error_fileds = ['__all__', 'login', 'password'];

    $("#signin_form").submit(function(e){
        var postData = $(this).serializeArray();
        var formURL = $(this).attr("action");
        $.ajax({
            url : formURL,
            type: "POST",
            data : postData,
            success:function(data, textStatus, jqXHR){
                if (data.result){
                    window.location = data.next_url;
                }
                else {
                    var error_holder = $('.error-holder');
                    error_holder.html('');
                    Object.keys(data.errors).forEach(function (item) {
                        error_holder.append('<div class="alert alert-danger" role="alert">' + data.errors[item] + '</div>');
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown){
                //if fails
            }
        });
        e.preventDefault(); //STOP default action
        e.unbind(); //unbind. to stop multiple form submit.
    });
});
