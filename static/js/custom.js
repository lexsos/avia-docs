$(document).ready(function() {
	var win = $(window);

	var getCookie = function (name) {
    	var cookieValue = null;
    	if (document.cookie && document.cookie !== '') {
        	var cookies = document.cookie.split(';');
        	for (var i = 0; i < cookies.length; i++) {
            	var cookie = jQuery.trim(cookies[i]);
            	// Does this cookie string begin with the name we want?
            	if (cookie.substring(0, name.length + 1) === (name + '=')) {
                	cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                	break;
            	}
        	}
    	}
    	return cookieValue;
	};

	var load_more = function () {
		var next_url = $('.publication_holder').attr('data_next_url');
		if (next_url){
			$('.load_progress').removeClass('hidden');
			$('.publication_holder').attr('data_next_url', null);
			$.ajax({
            	url : next_url,
            	type: "GET",
            	success:function(data, textStatus, jqXHR){
					$('.publication_holder').append(data.html);
					if (data.has_next){
						$('.publication_holder').attr('data_next_url', data.next_url);
					}
					$('.load_progress').addClass('hidden');
            	},
            	error: function(jqXHR, textStatus, errorThrown){
					$('.load_progress').addClass('hidden');
            	}
        	});
		}
	};

	load_more();

	win.scroll(function() {
		if ($(document).height() - win.height() == win.scrollTop()) {
            load_more();
		}
	});

	$('.publication_holder').on('click', '.signature-button' ,function(event){
		event.preventDefault();
		var publication = $(this).closest('.publication');
		var document_name = publication.attr('data_document_name');
		var url = publication.attr('data_url_signature');
		var publication_id = publication.attr('id');
		var modal = $('#signature_modal');
		modal.find('.document-name').html(document_name);
		modal.find('.signature_button').attr('data_url', url);
		modal.find('.signature_button').attr('data_pub_id', publication_id);
		modal.modal();
	});

	$('#signature_modal .signature_button').click(function () {
		var url = $(this).attr('data_url');
		var publication_id = $(this).attr('data_pub_id');
		var csrftoken = getCookie('csrftoken');
		$.ajax({
            	url : url,
            	type: "POST",
				beforeSend: function(request) {
    				request.setRequestHeader('X-CSRFToken', csrftoken);
  				},
            	success:function(data, textStatus, jqXHR){
					$('#'+publication_id).replaceWith(data.html);
					$('#signature_modal').modal('hide');
            	},
            	error: function(jqXHR, textStatus, errorThrown){
            	}
		});
	});
});
